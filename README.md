# Nest Compiler SDK

This repository contains the Docker configuration for setting up the environment required to develop and build
the [`Nest Compiler`](https://gitlab.gomsay.com/etri/nest-compiler) project. It includes all necessary tools and
libraries pre-installed in an Ubuntu 20.04 container.

## Docker Image Details

- Base Image: `ubuntu:20.04`
- Working Directory: `/root/dev`

### Installed Tools and Dependencies

- Clang (version 8)
- CMake
- Graphviz
- Libpng-dev
- Protobuf (libprotobuf-dev and protobuf-compiler)
- LLVM (version 8)
- Ninja build system
- Wget
- OpenCL headers
- Google Glog
- Boost libraries
- Double conversion library
- Libevent
- OpenSSL
- Gflags
- Jemalloc
- Pthread stubs
- OpenCL ICD Loader
- Cross compilers for AArch64 (gcc-aarch64-linux-gnu, g++-aarch64-linux-gnu)
- AWS CLI version 2
- Git
- Python 3 and pip3
- Python packages: numpy, decorator, attrs, pytest, onnx, scipy, onnxruntime (1.12.1), PyYAML (6.0)
- libomp-dev

The Docker image also sets Clang 8 as the default compiler for both C and C++.

## Getting Started

- [Glow Docker Image](https://gitlab.gomsay.com/etri/nest-compiler-sdk/-/tree/main/docker?ref_type=heads#glow-docker-image)


## Using the Project

### Continuous Integration and Deployment
This section explains how to utilize the CI/CD pipeline integrated with the `Nest Compiler` project for efficient version management and deployment.

#### Automated Version Tagging and Deployment

The `.gitlab-ci.yml` file is set up to handle automated version management:

```yml
  script:
    - docker push $CI_REGISTRY/$CI_REGISTRY_IMAGE:$CI_COMMIT_REF_NAME
    - docker tag $CI_REGISTRY/$CI_REGISTRY_IMAGE:$CI_COMMIT_REF_NAME $CI_REGISTRY/$CI_REGISTRY_IMAGE:latest
    - docker push $CI_REGISTRY/$CI_REGISTRY_IMAGE:latest
```

- **Commit to Main Branch**: Commits made to the main branch automatically trigger a pipeline that tags the build as `latest`.
- **Tag Creation in GitLab**: Creating a new tag in GitLab's `Code > Tags` section triggers a pipeline for that specific tag.
    - For instance, if you set a tag `1.0.0`, the pipeline will tag the build as `nest-compiler-sdk:1.0.0`.


### Nest Compiler Build Process
Using `Nest Compiler SDK` in Builds </br>

The `Nest Compiler` project's `build` pipeline utilizes Docker images from the `Nest Compiler SDK`.

```yml
.builds:
  stage: build
  image: $CI_REGISTRY/nest-compiler-sdk:latest
  before_script:
```
- **Specifying Tags in Builds**: For building the `Nest Compiler`, you can specify the Docker image either with the `latest` tag for the most recent build, or with a specific version tag like `1.0.0`. This ensures that you're always using the desired version of the SDK.

## CI/CD Pipeline

This project's CI/CD pipeline is divided into three distinct stages, each performing specific functions:

- `check`
- `build`: This stage is responsible for building the Docker image.
- `dist`: In this stage, the built Docker image is pushed to the container registry.

### Build Stage

The build stage involves the creation and preparation of the Docker image.

```yml
build:
  stage: build
  image: docker:latest
  before_script:
    - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD
  script:
    - docker build --cache-from $CI_REGISTRY/$CI_REGISTRY_IMAGE:latest --tag $CI_REGISTRY/$CI_REGISTRY_IMAGE:$CI_COMMIT_REF_NAME .
  tags:
    - etri
```

#### Variables
For more about GitLab CI/CD Variable, please refer to the [official documentation](https://docs.gitlab.com/ee/ci/variables/predefined_variables.html).

- `$CI_REGISTRY_IMAGE`: `nest-compiler-sdk` (Available only if the container registry is enabled for the project.)
- `$CI_COMMIT_REF_NAME`: The branch or tag name for which the project is built.

#### Details of build stage
- Logging into Docker Hub: We use the CI/CD variables `$CI_REGISTRY_USER` and `$CI_REGISTRY_PASSWORD` to log in to Docker Hub.
    - For using GitLab's container registry, modify the login command as follows:
        - `docker login registry.gitlab.com -u ${CI_REGISTRY_USER} -p ${CI_REGISTRY_PASSWORD}`

- Build Caching: We utilize the latest tagged image from previous builds as a cache to enhance build speed.
- Version Tagging: The built image is tagged with `$CI_REGISTRY/$CI_REGISTRY_IMAGE:$CI_COMMIT_REF_NAME` for efficient version management.
- `etri` Tag: This tag ensures that the pipeline is executed only on the specified `GitLab Runner`.

### dist
In the `dist` stage, the Docker image created in the `build` stage is pushed to the container registry.


```yml
dist:
  stage: dist
  image: docker:latest
  needs: [build]
  before_script:
    - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD
  script:
    - docker push $CI_REGISTRY/$CI_REGISTRY_IMAGE:$CI_COMMIT_REF_NAME
    - docker tag $CI_REGISTRY/$CI_REGISTRY_IMAGE:$CI_COMMIT_REF_NAME $CI_REGISTRY/$CI_REGISTRY_IMAGE:latest
    - docker push $CI_REGISTRY/$CI_REGISTRY_IMAGE:latest
  tags:
    - etri
```

#### Details of dist stage
- The Docker image, a result of the build stage, is pushed to the registry.
- Initially, the image is pushed using the branch or tag name (`$CI_COMMIT_REF_NAME`).
- Subsequently, the same image is tagged as `latest` and pushed again to keep it updated to the latest version.